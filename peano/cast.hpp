#ifndef PEANO_CAST_HPP
#define PEANO_CAST_HPP
#include <cinttypes>
#include <type_traits>

#include "base.hpp"

namespace peano {
    template <typename N, typename T = uint64_t>
    struct to_integral;

    template <typename T, typename IC>
    struct _from_integral;

    template <typename T, T N>
    struct from_integral: _from_integral<T, std::integral_constant<T, N>> {};

    template <typename N, typename T = uint64_t>
    constexpr T to_integral_v = to_integral<N, T>::value;

    template <typename T = uint64_t, T val = 0>
    using from_integral_t = typename from_integral<T, val>::type;

    template <typename T>
    struct to_integral<zero, T> {
        constexpr static T value = 0;
    };

    template <typename P, typename T>
    struct to_integral<next<P>, T> {
        constexpr static T value = to_integral_v<P, T> + 1;
    };

    template <typename T>
    struct _from_integral<T, std::integral_constant<T, 0>> {
        using type = zero;
    };

    template <typename T, T N>
    struct _from_integral<
        T,
        std::integral_constant<std::enable_if_t<N != 0, T>, N>> {
        using type = next<from_integral_t<T, N - 1>>;
    };
}  // namespace peano
#endif
