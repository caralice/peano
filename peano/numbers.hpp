#ifndef PEANO_NUMBERS_HPP
#define PEANO_NUMBERS_HPP
#include "base.hpp"

namespace peano {
    using one   = next<zero>;
    using two   = next<one>;
    using three = next<two>;
    using four  = next<three>;
    using five  = next<four>;
    using six   = next<five>;
    using seven = next<six>;
    using eight = next<seven>;
    using nine  = next<eight>;
    using ten   = next<nine>;
}  // namespace peano
#endif
