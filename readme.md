# Compile-time Peano arithmetic

## Example

```cpp
#include <type_traits>
#include "peano/base.hpp"
#include "peano/cast.hpp"
#include "peano/numbers.hpp"

using namespace peano;

int main() {
    // 3 * (2 + 1) + 4 * 4 == 5 * (3 + 2)
    static_assert(
        std::is_same<
            sum_t<mul_t<three, next<two>>, mul_t<four, four>>,
            from_integral_t<uint8_t, 5 * to_integral_v<sum_t<three, two>>>
        >::value,
        "Math doesn't work"
    );
}
```

## Reference

Everything is in
the `peano` namespace.Numbers are represented as types.

### `peano/base.hpp`

Header providing basic operations.

#### `zero`

Represents the zero number.

#### `next<P>`

The number after `P`.

#### `prev<N>`

The number before `P`.

#### `sum<A, B>`

A struct containing the result of `A + B`.
To get the actual result you can use `sum<A, B>::result` or `sum_t<A, B>`.

#### `mul<A, B>`

A struct containing the result of `A * B`.
To get the actual result use `mul<A, B>::result` or `mul_t<A, B>`.

#### `peano/numbers.hpp`

Header containing some predefined numbers:

- `two`
- `three`
- `four`
- `five`
- `six`
- `seven`
- `eight`
- `nine`
- `ten`

### `peano/cast.hpp`

Header providing converting between Peano types and integral
values(obviously `constexpr`s).

#### `from_integral<T, T N>`

A struct containing the `N` converted to Peano type.
To get the actual result use `from_integral<T, N>::type` or `from_integral_t<T, N>`.

#### `to_integral<N, T>`

A struct containing the Peano `N` number converted to an integral of type `T`.
To get the actual integral use `to_integral<N, T>::value` or `to_integral_v<N, T>`.
