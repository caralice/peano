#include <iostream>

#include "peano/cast.hpp"
#include "peano/numbers.hpp"

using namespace peano;

int main() {
    static_assert(std::is_same<sum_t<one, one>, two>::value, "1 + 1 != 2");
    static_assert(std::is_same<sum_t<zero, three>, three>::value, "0 + 3 != 3");
    static_assert(!std::is_same<sum_t<one, one>, three>::value, "1 + 1 == 3");
    static_assert(std::is_same<mul_t<one, five>, five>::value, "1 * 5 != 5");
    static_assert(std::is_same<sum_t<mul_t<three, three>, mul_t<four, four>>,
                               mul_t<five, five>>::value,
                  "3² + 4² != 5²");
    static_assert(std::is_same<from_integral_t<uint64_t, 0>, zero>::value,
                  "0 != 0");
    static_assert(to_integral_v<from_integral_t<uint64_t, 500>> == 500);
}
